# Executing conda

```shell script
build.sh 
run.sh
```
The last command is going to execute conda environment with docker.


## How to start on?

### Create a new  environment

```shell script
  conda create --name data_science
```

### Activate environment

```shell script
conda init bash
```

Then:
```shell script
conda activate data_science
```

### Install dependencies

We can install dependencies by:

1. Update our Dockerfile
2. Executing `conda install`.

```shell script
conda install jupyter notebook jupyterlab
```

## Open jupyter notebook

1. Open a terminal and run:

```shell script
docker exec -ti acamica /bin/bash
jupyter notebook list
```

2. Go to: http://127.0.0.1:8888?token=asdasdksag

The token is provided in the command executed in the step 1.

